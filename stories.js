let urlParams = new URLSearchParams(window.location.search);

function changeListing(name) {

    fetch("/stories/" + name + "/info.json")
        .then(
            function(response) {
                response.json().then(function(data) {
                    document.querySelector("#story-description").innerHTML = data.description;
                    document.querySelector("#story-title").innerHTML = data.title;
                    document.querySelector("#story-download").href = "/stories/" + name + "/" + data.file;
                });
            }
        );


    window.history.replaceState( {} , name, "stories.html?name=" + name );
    urlParams = new URLSearchParams(window.location.search); // This obviously won't get automatically updated.
}

async function moveListing(direction, name) {
    // true = forward
    // false = backwards


    fetch("/stories/folders.json")
        .then(function(response) {
            response.json().then(function(data) {
                let current = urlParams.get("name") // current listing
                let currentIndex = data.indexOf(current);

                console.log(currentIndex);

                if (direction == null || currentIndex == -1) { // From loadListing
                    if (name != null && currentIndex != -1) {
                        _hideShowNav(currentIndex, data.length);
                        changeListing(name);
                        return;
                    }

                    console.log("Here");

                    _hideShowNav(0, data.length);
                    changeListing(data[0]);
                    return;
                }

                else if (direction) {
                    // Forward
                    currentIndex++;
                }
                else {
                    // Backward
                    currentIndex--;
                }
                _hideShowNav(currentIndex, data.length);
                changeListing(data[currentIndex]);


                console.log(currentIndex);


            });
        });
}

async function _hideShowNav(index, len) {

    if (index == 0) {
        document.querySelector("#previous").style.display =  "none";
        document.querySelector("#next").style.display =  "block";
    }
    else if (index == len - 1) {
        document.querySelector("#previous").style.display =  "block";
        document.querySelector("#next").style.display =  "none";
    }
    else {
        document.querySelector("#previous").style.display =  "block";
        document.querySelector("#next").style.display =  "block";
    }


}

function loadListing() {
    if (urlParams.get("name") && urlParams.get("name") != "undefined") {
        console.log("Here");
        moveListing(null, urlParams.get("name"))
    }
    else {
        console.log("Here");
        moveListing(null);
    }


    var isMobile = window.matchMedia("(max-width: 600px)");
    if (isMobile.matches) {
        document.querySelector("#nav-back").classList.add("citation");
    }

}
